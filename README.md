# Especialización en desarrollo FullStack
Desarrollo FullStack por la universidad Austral, cursado en Coursera.

## Diseñando páginas web con Bootstrap 4 - Evaluación del proyecto - Módulo 2

### Ejercicios
1. Construir elementos de navegación en un sitio web.
2. Mostrar información utilizando tablas, tarjetas y media.
3. Construir formularios y botones para interacturar con los usuarios

---

### Web sobre libros de programación
Una página web responsive sobre los mejores libros de programación y dónde poder adquirlos en España

### Tecnologías
- HTML / CSS / JS
- Bootstrap 4
- NodeJS
- Visual Studio Code
- Git
- Bitbucket